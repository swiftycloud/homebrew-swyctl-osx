class Swyctl < Formula
  desc "A command line tool to retrieve local weather"
  homepage "https://swifty.cloud"
  url "https://gitlab.com/swiftycloud/swyctl/blob/master/archive/swyctl"
  sha256 "f1e293f69cc0e6e884b55e57ba47cb8dddbbe98fdfa5f6d57151865b8d061db0"
  version "1.0.0"

  #depends_on "curl"

  bottle :unneeded

  def install
    bin.install "swyctl"
  end
end
